package playground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class TimeSeriesPanel extends JPanel
{

    public TimeSeriesPanel()
    {
        JToolBar toolBar = new JToolBar();
        JPanel flowPanel = new JPanel();
        TextField countTextField = new TextField();
        JButton setupButton = new JButton("Nastavit");
        JButton exportButton = new JButton("Export");
        toolBar.add(countTextField);
        toolBar.add(setupButton);
        toolBar.add(exportButton);

        setLayout(new BorderLayout());
        add(toolBar, BorderLayout.NORTH);
        add(flowPanel, BorderLayout.CENTER);

        flowPanel.setLayout(new FlowLayout());
        List<TextField> textFields = new ArrayList<>();

        setupButton.addActionListener((e)->{
            int count = Integer.parseInt(countTextField.getText());
            for(int i=0; i<count; i++)
            {
                TextField t =new TextField();
                flowPanel.add(t);
                textFields.add(t);
            }
        });

        exportButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(TextField f : textFields)
                {
                    System.out.println(f.getText());
                }
            }
        });


    }
}
