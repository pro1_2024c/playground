package playground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class DrawingPanel extends JPanel
{
    public DrawingPanel()
    {
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                drawPoint(e.getX(), e.getY());
            }
        });
    }

    public void drawPoint(int x, int y) {
        Graphics2D g = (Graphics2D) getGraphics();
        g.setColor(Color.BLUE);
        g.fillOval(x, y, 10, 10);
    }
}
