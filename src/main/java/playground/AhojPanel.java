package playground;

import javax.swing.*;
import java.awt.*;

public class AhojPanel extends JPanel
{
    public AhojPanel()
    {
        setLayout(new BorderLayout());
        
        JButton leftButton = new JButton("Vlevo");
        leftButton.setBackground(Color.yellow);
        add(leftButton, BorderLayout.WEST);

        add(new JButton("Vpravo"), BorderLayout.EAST);
        add(new JButton("Nahoře"), BorderLayout.NORTH);
        add(new JButton("Dole"), BorderLayout.SOUTH);

    }
}
